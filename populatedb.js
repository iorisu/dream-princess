#! /usr/bin/env node

var userArgs = process.argv.slice(2);
if (!userArgs[0].startsWith("mongodb://")) {
  console.log(
    "ERROR: You need to specify a valid mongodb URL as the first argument"
  );
  return;
}

var async = require("async");
var Clothe = require("./models/clothes");
var User = require("./models/user");
var Princess = require("./models/princess");

var mongoose = require("mongoose");
var mongoDB = userArgs[0];
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
mongoose.connection.on(
  "error",
  console.error.bind(console, "MongoDB connection server")
);

var clothe = [];
var user = [];

//Clothes
function clothesCreate(name, type, color, imageUrl, cb) {
  this.clothesDetail = {
    name: name,
    type: type,
    color: color,
    imageUrl: imageUrl
  };
  var clothe = new Clothes(clothesDetail);

  clothe.save(function(err) {
    if (err) {
      cb(err, null);
      return;
    }
    console.log("New Clothes: " + clothe);
    clothes.push(clothe);
    cb(null, clothe);
  });
}

//User
function userCreate(name, email, password) {
  this.userDetail = {
    name: name,
    email: email,
    password: password
  };
  var user = new User(userDetail);

  user.save(function(err) {
    if (err) {
      cb(err, null);
      return;
    }
    console.log("New User: " + user);
    users.push(user);
    cb(null, user);
  });
}

//Princess
function princessCreate(
  namePrincess,
  skinColor,
  eyeColor,
  hairColor,
  costumes,
  head,
  middle,
  bottom,
  shoes,
  wardrobe,
  clothes
) {
  this.princessDetail = {
    namePrincess: namePrincess,
    skinColor: skinColor,
    eyeColor: eyeColor,
    hairColor: hairColor,
    wardrobe: wardrobe
  };
  this.costumesDetail = {
    head: head,
    middle: middle,
    bottom: bottom,
    shoes: shoes
  };
  var princess = new Princess(princessDetail, costumesDetail);

  princess.save(function(err) {
    if (err) {
      cb(err, null);
      return;
    }
    console.log("New Princess: " + princess);
    princesss.push(princess);
    cb(null, princess);
  });
}

async.series(
  [createClothes, createUser, createPrincess],
  //callback
  function(err, results) {
    if (err) {
      console.log("FINAL ERR: " + err);
    } else {
      console.log("BOOKInstances: " + bookinstances); // cos zmienic
    }
    // All done, disconnect from database
    mongoose.connection.close();
  }
);
