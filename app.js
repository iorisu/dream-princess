var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var assert = require("assert");
//
var mongoDb = "mongodb://admin:admin@ds117010.mlab.com:17010/dreamprincess";
var session = require("express-session");
var MongoStore = require("connect-mongo")(session);
var app = express();
//
//
var session = require("express-session");
var MongoDB = process.env.MongoDB_URI || mongoDb;
mongoose.connect(MongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDb connection error."));
db.once("open", function() {
  // we're connected!
});

app.use(
  session({
    secret: "work hard",
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: db
    })
  })
);

app.use(function(req, res, next) {
  req.db = db;
  next();
});

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(express.static(__dirname + "/public"));
//

var indexRouter = require("./routes/index");

app.use(express.static(__dirname + "/node_modules/bootstrap/dist"));
app.use(express.static(__dirname + "../public/images/character/clothes/head"));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use("/", indexRouter);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err
    });
  });
}
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {}
  });
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

//aggregate
var AgregateRanking = function(db, callback) {
  var collection = db.collection("users");
  collection.aggregate(
    [
      {
        $group: {
          _id: {
            sumRanking: {
              $sum: ["$style", "$creativity", "$Intelligence", "$Condition"]
            }
          }
        }
      }
    ],
    function(err, results) {
      assert.equal(err, null);

      console.log(results);
      callback(results);
    }
  );
};

module.exports = app;
