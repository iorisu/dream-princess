var User = require("../models/user");
var Clothes = require("../models/clothes");

exports.user_create_signin_post = function(req, res, next) {
  // confirm that user typed same password twice
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error("Passwords do not match.");
    err.status = 400;
    res.send("passwords dont match");
    return next(err);
  }
  if (
    req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf
  ) {
    var userData = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      passwordConf: req.body.passwordConf
    };
    User.create(userData, function(error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        return res.redirect("/profile");
      }
    });
  } else if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function(
      error,
      user
    ) {
      if (error || !user) {
        var err = new Error("Wrong email or password.");
        err.status = 401;
        return next(err);
      } else {
        req.session.userId = user._id;
        return res.redirect("/profile");
      }
    });
  } else {
    var err = new Error("All fields required.");
    err.status = 400;
    return next(err);
  }
};

exports.user_profile_application_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("userprofile", {
          user: user,
          login: true,
          princess: true
        });
      }
    }
  });
};

exports.user_change_email_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("changeemail", { user: user, login: true });
      }
    }
  });
};

exports.user_change_email_post = function(req, res, next) {
  User.findByIdAndUpdate(
    req.session.userId,
    req.body,
    { new: true },
    (err, email) => {
      console.log(req.body + req.session.userId);
      if (err) return res.status(500).send(err);
      User.findById(req.session.userId).exec(function(error, user) {
        if (error) {
          return next(error);
        } else {
          if (user === null) {
            var err = new Error("Not authorized! Go back!");
            err.status = 400;
            return next(err);
          } else {
            return res.render("userprofile", { user: user, login: true });
          }
        }
      });
    }
  );
};

exports.user_delete_account_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("deleteaccount", { user: user, login: true });
      }
    }
  });
};

exports.user_delete_account_post = function(req, res, next) {
  User.findByIdAndRemove(req.session.userId, (err, user) => {
    console.log("sprawdzenie czy jest poprawnie");
    user.save((err, user) => {
      res.render("delete");
      if (err) {
        console.log(err);
      }
    });
    if (err) {
      console.log(err);
    }
  });
};

exports.user_admin_event_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        return res.render("admin", { user: user, login: true });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

//admin-clothes
exports.user_admin_clothes_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        Clothes.find().exec(function(err, clothes) {
          return res.render("adminClothes", {
            user: user,
            clothes: clothes,
            login: true
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_create_clothe_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        Clothes.find().exec(function(err, clothes) {
          return res.render("adminClothesAddNew", {
            user: user,
            clothes: clothes,
            login: true
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_create_clothe_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (
      req.body.name &&
      req.body.type &&
      req.body.color &&
      req.body.imageUrl &&
      req.body.imageUrlMin &&
      req.body.style &&
      req.body.creativity &&
      req.body.Intelligence &&
      req.body.Condition
    ) {
      var clotheData = {
        name: req.body.name,
        type: req.body.type,
        color: req.body.color,
        imageUrl: req.body.imageUrl,
        imageUrlMin: req.body.imageUrlMin,
        style: req.body.style,
        creativity: req.body.creativity,
        Intelligence: req.body.Intelligence,
        Condition: req.body.Condition
      };
      Clothes.create(clotheData, function(error, clothe) {
        if (error) {
          return next(error);
        } else {
          return res.render("admin", {
            clothe: clothe,
            user: user,
            login: true
          });
        }
      });
    }
  });
};

//
exports.clothe_see_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        return res.render("adminClothes", {
          user: user,
          clothes: clothes,
          login: true
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

//
exports.clothe_see_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (req.body.id_clothe) {
      var clotheData = {
        _id: req.body.id_clothe
      };
      Clothes.findById(clotheData).exec(function(error, clothe) {
        if (error) {
          return next(error);
        } else {
          console.log(clotheData);
          return res.render("showClothe", {
            clothe: clothe,
            user: user,
            login: true
          });
        }
      });
    }
  });
};

//
exports.clothe_edit_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        Clothes.find().exec(function(err, clothes) {
          return res.render("adminClothes", {
            user: user,
            clothes: clothes,
            login: true
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

//
exports.clothe_edit_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (req.body.id_clothe) {
      var clotheData = {
        _id: req.body.id_clothe
      };
      Clothes.findById(clotheData).exec(function(error, clothe) {
        if (error) {
          return next(error);
        } else {
          console.log(clotheData);
          return res.render("adminClothesEdit", {
            clothe: clothe,
            user: user,
            login: true
          });
        }
      });
    }
  });
};

exports.admin_clothe_save_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        Clothes.find().exec(function(error, clothes) {
          return res.render("adminSaveEditCostume", {
            user: user,
            login: true,
            clothes: clothes
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_clothe_save_edit_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (req.body.id_clothe) {
      var clotheData = {
        _id: req.body.id_clothe
      };
      Clothes.findByIdAndUpdate(clotheData, {
        $set: {
          name: req.body.name,
          color: req.body.color,
          imageUrl: req.body.imageUrl,
          imageUrlMin: req.body.imageUrlMin,
          style: req.body.style,
          creativity: req.body.creativity,
          Intelligence: req.body.Intelligence,
          Condition: req.body.Condition
        }
      }).exec(function(err, clothe) {
        if (err) {
          console.log(err);
          req.status(500).send(err);
        } else {
          return res.render("adminSaveEditCostume.ejs", {
            user: user,
            login: true,
            clothe: clothe
          });
        }
      });
    }
  });
};

exports.clothe_delete_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (req.body.id_clothe) {
      var clotheData = {
        _id: req.body.id_clothe
      };
      Clothes.findByIdAndRemove(clotheData, (err, clothe) => {
        console.log("sprawdzenie czy jest poprawnie");
        user.save((err, clothe) => {
          res.render("adminClotheDelete");
          if (err) {
            console.log(err);
          }
        });
        if (err) {
          console.log(err);
        }
      });
    }
  });
};

exports.clothe_delete_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        return res.render("adminClotheDelete", {
          user: user,
          login: true
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

//admin z działem użytkowników
//get'y
exports.admin_user_details_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        User.find().exec(function(err, character) {
          return res.render("adminUserInfo", {
            user: user,
            character: character,
            login: true
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_edit_user_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        User.find().exec(function(err, character) {
          return res.render("adminUserEdit", {
            user: user,
            character: character,
            login: true
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_delete_user_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        return res.render("adminUserDelete", {
          user: user,
          login: true
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_edit_user_save_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        User.find().exec(function(error, character) {
          return res.render("adminSaveEditUser", {
            user: user,
            login: true,
            character: character
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};

exports.admin_delete_user_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    var characterData = {
      _id: req.body.id_character
    };
    console.log(characterData);
    User.findByIdAndRemove(characterData, (err, character) => {
      console.log("sprawdzenie czy jest poprawnie");
      user.save((err, character) => {
        res.render("adminUserDelete");
        console.log(characterData);
        if (err) {
          console.log(err);
        }
      });
      if (err) {
        console.log(err);
      }
    });
  });
};

exports.admin_user_details_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    var characterData = {
      _id: req.body.id_character
    };
    User.findById(characterData).exec(function(error, character) {
      if (error) {
        return next(error);
      } else {
        console.log(characterData);
        return res.render("adminUserInfo", {
          character: character,
          user: user,
          login: true
        });
      }
    });
  });
};

exports.admin_edit_user_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    var characterData = {
      _id: req.body.id_character
    };
    User.findById(characterData).exec(function(error, character) {
      if (error) {
        return next(error);
      } else {
        console.log(characterData);
        return res.render("adminUserEdit", {
          character: character,
          user: user,
          login: true
        });
      }
    });
  });
};

exports.admin_edit_send_user_post = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    var characterData = {
      _id: req.body.id_character
    };
    User.findByIdAndUpdate(characterData, {
      $set: {
        username: req.body.username,
        email: req.body.email,
        accountType: req.body.accountType,
        namePrincess: req.body.namePrincess,
        dpMoney: req.body.dpMoney
      }
    }).exec(function(err, character) {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      } else {
        return res.render("adminSaveEditUser.ejs", {
          user: user,
          login: true,
          character: character
        });
      }
    });
  });
};

//admin-user
exports.user_admin_users_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else if (user.accountType == "admin") {
        User.find().exec(function(err, users) {
          return res.render("adminUser", {
            user: user,
            users: users,
            login: true
          });
        });
      } else {
        var err = new Error("Nie masz wystarczających praw!");
        err.status = 400;
      }
    }
  });
};
