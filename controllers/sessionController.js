var User = require("../models/user");

exports.session_profile = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        req.session.userId = user._id;
        return res.render("newuser.ejs", {
          user: user,
          login: true
        });
      }
    }
  });
};

exports.session_destroy = function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect("/");
      }
    });
  }
};
