var User = require("../models/user");

exports.city_home_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("city", {
          user: user,
          login: true
        });
      }
    }
  });
};

exports.city_libraty_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("cityLibrary", {
          user: user,
          login: true
        });
      }
    }
  });
};

exports.city_shopping_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("cityShopping", {
          user: user,
          login: true
        });
      }
    }
  });
};

exports.city_atm_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("cityATM", {
          user: user,
          login: true
        });
      }
    }
  });
};

exports.buy_glasses_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      glassesBuy: req.body.glassesBuy,
      dpMoney: req.body.dpMoney
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      return res.render("cityBought.ejs", {
        user: user,
        login: true
      });
    }
  });
};

exports.buy_socks_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      socksBuy: req.body.socksBuy,
      dpMoney: req.body.dpMoney
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      return res.render("cityBought.ejs", {
        user: user,
        login: true
      });
    }
  });
};

exports.first_atm_post = function(req, res, next) {
  var today = new Date(Date.now());
  var tomorrow = new Date();
  tomorrow.setDate(today.getDate() + 1);
  tomorrow.setHours(0, 0, 0, 0);
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      letsMoney: tomorrow,
      dpMoney: req.body.dpMoney
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      return res.render("cityATMsave.ejs", {
        user: user,
        login: true
      });
    }
  });
};

exports.first_library_post = function(req, res, next) {
  var today = new Date(Date.now());
  var tomorrow = new Date();
  tomorrow.setDate(today.getDate() + 1);
  tomorrow.setHours(0, 0, 0, 0);
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      letsPlay: tomorrow,
      Intelligence: req.body.Intelligence
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      return res.render("cityLibrarysave.ejs", {
        user: user,
        login: true
      });
    }
  });
};

exports.atm_save_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("cityATMsave.ejs", {
          user: user,
          login: true
        });
      }
    }
  });
};
