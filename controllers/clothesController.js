var User = require("../models/user");
var Clothes = require("../models/clothes");

exports.clothes_wardrobe_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        return res.render("wardrobe", { user: user, login: true });
      }
    }
  });
};

exports.clothes_wardrobe_head = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        Clothes.find({ type: { $eq: "head" } }).exec(function(err, clothe) {
          if (err) {
            return next(err);
          }
          res.render("clothes", { user: user, clothes: clothe, login: true });
        });
      }
    }
  });
};

exports.clothes_wardrobe_middle = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        Clothes.find({ type: { $eq: "middle" } }).exec(function(err, clothe) {
          if (err) {
            return next(err);
          }
          res.render("clothes", { user: user, clothes: clothe, login: true });
        });
      }
    }
  });
};

exports.clothes_wardrobe_bottom = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        Clothes.find({ type: { $eq: "bottom" } }).exec(function(err, clothe) {
          if (err) {
            return next(err);
          }
          res.render("clothes", { user: user, clothes: clothe, login: true });
        });
      }
    }
  });
};

exports.clothes_wardrobe_shoes = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        Clothes.find({ type: { $eq: "shoes" } }).exec(function(err, clothe) {
          if (err) {
            return next(err);
          }
          res.render("clothes", { user: user, clothes: clothe, login: true });
        });
      }
    }
  });
};

exports.clothes_wardrobe_glasses = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        Clothes.find({ type: { $eq: "glasses" } }).exec(function(err, clothe) {
          if (err) {
            return next(err);
          }
          res.render("clothes", { user: user, clothes: clothe, login: true });
        });
      }
    }
  });
};

exports.clothes_wardrobe_socks = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        Clothes.find({ type: { $eq: "sock" } }).exec(function(err, clothe) {
          if (err) {
            return next(err);
          }
          res.render("clothes", { user: user, clothes: clothe, login: true });
        });
      }
    }
  });
};
