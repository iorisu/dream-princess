var User = require("../models/user");
var Princess = require("../models/princess");

exports.princess_create_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        console.log(user);
        if (user.namePrincess === undefined) {
          isLoggedIn: true;
          req.session.userId = user._id;
          res.render("creatingprincess.ejs", {
            user: user,
            login: true,
            princess: false
          });
        } else if (user.bottom === undefined) {
          console.log(user.namePrincess);
          isLoggedIn: true;
          req.session.userId = user._id;
          res.render("creatingclothecreate.ejs", {
            user: user,
            login: true,
            princess: false
          });
        } else {
          console.log(user);
          req.session.userId = user._id;
          isLoggedIn: true;
          return res.render("princessHomepage.ejs", {
            user: user,
            login: true,
            princess: true
          });
        }
      }
    }
  });
};

exports.princess_create_post = function(req, res, next) {
  if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function(
      error,
      user
    ) {
      console.log(user + "post create");
      if (error || !user) {
        var err = new Error("Wrong email or password.");
        err.status = 401;
        return next(err);
      } else {
        console.log(user);
        if (user.namePrincess === undefined) {
          console.log("1");
          isLoggedIn: true;
          req.session.userId = user._id;
          res.render("creatingprincess.ejs", {
            user: user,
            login: true
          });
        } else if (user.bottom === undefined) {
          isLoggedIn: true;
          console.log("2");
          req.session.userId = user._id;
          res.render("creatingclothecreate.ejs", {
            user: user,
            login: true,
            princess: false,
            clothes: true
          });
        } else {
          console.log("3");
          req.session.userId = user._id;
          return res.render("princessHomepage.ejs", {
            user: user,
            login: true
          });
        }
      }
    });
  } else {
    var err = new Error("All fields required.");
    err.status = 400;
    return next(err);
  }
};

exports.princess_about_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (error) {
          return next(error);
        } else {
          if (user === null) {
            var err = new Error("cos poszło nie tak");
            return next(err);
          } else {
            console.log(user);
            return res.render("princessHomepage.ejs", {
              user: user,
              login: true,
              princess: true
            });
          }
        }
      }
    }
  });
};

exports.princess_about_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      head: req.body.head,
      middle: req.body.middle,
      bottom: req.body.bottom,
      shoes: req.body.shoes
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      return res.render("princessHomepage.ejs", {
        user: user,
        login: true,
        princess: true
      });
    }
  });
};

exports.princess_about_homepage_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      head: req.body.head,
      middle: req.body.middle,
      bottom: req.body.bottom,
      shoes: req.body.shoes
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      return res.render("newprincessdone.ejs", {
        user: user,
        login: true,
        princess: true
      });
    }
  });
};

//Przy tworzeniu postaci OK
exports.princess_create_costume_register_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user.namePrincess === null) {
          var err = new Error("cos poszło nie tak");
          return next(err);
        } else {
          console.log(user);
          return res.render("creatingclothecreate.ejs", {
            user: user,
            login: true,
            princess: true,
            clothes: true
          });
        }
      }
    }
  });
};

//OK
exports.princess_create_costume_register_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      namePrincess: req.body.namePrincess,
      skinColor: req.body.skinColor,
      eyeColor: req.body.eyeColor,
      hairColor: req.body.hairColor
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      console.log(user);
      return res.render("empiria.ejs", {
        user: user,
        login: true,
        princess: true,
        clothes: true
      });
    }
  });
};

//Przy przebraniu postaci - tworzenie bohaterki
exports.princess_create_costume_register_step3_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user.namePrincess === null) {
          var err = new Error("cos poszło nie tak");
          return next(err);
        } else {
          console.log(user);
          return res.render("creatingclothecreate.ejs", {
            user: user,
            login: true,
            princess: true,
            clothes: true
          });
        }
      }
    }
  });
};

exports.princess_create_costume_register_step3_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      head: req.body.head,
      middle: req.body.middle,
      bottom: req.body.bottom,
      shoes: req.body.shoes,
      style: req.body.style,
      creativity: req.body.creativity
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      console.log(user);
      return res.render("newprincessdone.ejs", {
        user: user,
        login: true,
        princess: true,
        clothes: true
      });
    }
  });
};

//Przy przebraniu postaci
exports.princess_edit_costume_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user.namePrincess === null) {
          var err = new Error("cos poszło nie tak");
          return next(err);
        } else {
          console.log(user);
          return res.render("creatingclothe.ejs", {
            user: user,
            login: true,
            princess: true,
            clothes: true
          });
        }
      }
    }
  });
};

exports.princess_edit_costume_post = function(req, res, next) {
  User.findByIdAndUpdate(req.session.userId, {
    $set: {
      head: req.body.head,
      middle: req.body.middle,
      bottom: req.body.bottom,
      shoes: req.body.shoes,
      style: req.body.style,
      creativity: req.body.creativity,
      glasses: req.body.glasses,
      socks: req.body.socks,
      Intelligence: req.body.Intelligence,
      Condition: req.body.Condition
    }
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      console.log(user);
      return res.render("saveeditcostume.ejs", {
        user: user,
        login: true,
        princess: true,
        clothes: true
      });
    }
  });
};

//
exports.princess_create_empiria_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          //zmiana na wartośc jakiegoś statu
          console.log("Empiria", user);
          return res.render("empiria.ejs", {
            user: user,
            login: true
          });
        }
      }
    }
  });
};

//
exports.princess_create_princess_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          //zmiana na wartośc jakiegoś statu
          return res.render("newprincessdone.ejs", {
            user: user,
            login: true
          });
        }
      }
    }
  });
};

exports.princess_save_edit_costume_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          //zmiana na wartośc jakiegoś statu
          return res.render("saveeditcostume.ejs", {
            user: user,
            login: true
          });
        }
      }
    }
  });
};

//ranking
exports.ranking_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          User.find()
            .sort([["style", -1]])
            .exec(function(err, data) {
              if (err) {
                return next(err);
              }
              console.log(data);
              res.render("ranking.ejs", {
                user: user,
                data: data,
                login: true,
                ranking: true
              });
            });
        }
      }
    }
  });
};

//ranking styl
exports.ranking_style_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          User.find()
            .sort([["style", -1]])
            .exec(function(err, data) {
              if (err) {
                return next(err);
              }
              console.log(data);
              res.render("ranking.ejs", {
                user: user,
                data: data,
                login: true,
                ranking: true
              });
            });
        }
      }
    }
  });
};

//ranking kreatywnosc
exports.ranking_creativity_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          User.find()
            .sort([["creativity", -1]])
            .exec(function(err, data) {
              if (err) {
                return next(err);
              }
              console.log(data);
              res.render("ranking.ejs", {
                user: user,
                data: data,
                login: true,
                ranking: true
              });
            });
        }
      }
    }
  });
};

//ranking inteligencja
exports.ranking_intelligence_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          User.find()
            .sort([["Intelligence", -1]])
            .exec(function(err, data) {
              if (err) {
                return next(err);
              }
              console.log(data);
              res.render("ranking.ejs", {
                user: user,
                data: data,
                login: true,
                ranking: true
              });
            });
        }
      }
    }
  });
};

//ranking kondycja
exports.ranking_condition_get = function(req, res, next) {
  User.findById(req.session.userId).exec(function(error, user) {
    if (error) {
      return next(error);
    } else {
      if (user === null) {
        var err = new Error("Not authorized! Go back!");
        err.status = 400;
        return next(err);
      } else {
        if (user !== null) {
          User.find()
            .sort([["Condition", -1]])
            .exec(function(err, data) {
              if (err) {
                return next(err);
              }
              console.log(data);
              res.render("ranking.ejs", {
                user: user,
                data: data,
                login: true,
                ranking: true
              });
            });
        }
      }
    }
  });
};
