var express = require("express");
var router = express.Router();
var User = require("../models/user");
var Princess = require("../models/princess");

var sessionController = require("../controllers/sessionController");
var userController = require("../controllers/userController");
var princessController = require("../controllers/princessController");
var clothesController = require("../controllers/clothesController");
var cityController = require("../controllers/cityController");

router.get("/", function(req, res) {
  res.render("index", { login: false });
});

router.get("/registration", function(req, res) {
  res.render("registration", { login: false });
});

router.post("/", userController.user_create_signin_post);

router.get("/delete", function(req, res, next) {
  res.render("delete");
});

//account_user
router.get("/profile", sessionController.session_profile);
router.get("/logout", sessionController.session_destroy);
router.get(
  "/homepage/user-profile",
  userController.user_profile_application_get
);
router.get("/change-email", userController.user_change_email_get);
router.post("/change-email", userController.user_change_email_post);
router.get("/delete-account", userController.user_delete_account_get);
router.post("/delete-user", userController.user_delete_account_post);

//admin
router.get("/admin", userController.user_admin_event_get);
router.get("/admin/clothes", userController.user_admin_clothes_get);
router.get("/admin/user-list", userController.user_admin_users_get);
router.get("/admin/clothes-new", userController.admin_create_clothe_get);
router.post("/admin/clothes-new", userController.admin_create_clothe_post);
router.get("/admin/clothes-details", userController.clothe_see_get);
router.post("/admin/see-clothe", userController.clothe_see_post);
router.get("/admin/clothes-edit", userController.clothe_edit_get);
router.post("/admin/edit-clothe", userController.clothe_edit_post);
router.get("/admin/delete-clothe", userController.clothe_delete_get);
router.post("/admin/delete-clothe", userController.clothe_delete_post);
router.get("/admin/edit-clothe-save", userController.admin_clothe_save_get);
router.post(
  "/admin/save-edit-clothe",
  userController.admin_clothe_save_edit_post
);
router.get("/admin/see-user", userController.admin_user_details_get);
router.get("/admin/edit-user", userController.admin_edit_user_get);
router.get("/admin/delete-user", userController.admin_delete_user_get);
router.post("/admin/user-details", userController.admin_user_details_post);
router.post("/admin/edit-user", userController.admin_edit_user_post);
router.post("/admin/delete-user", userController.admin_delete_user_post);
router.post("/admin/edit-send-user", userController.admin_edit_send_user_post);
router.get("/admin/edit-user-save", userController.admin_edit_user_save_get);

//account_princess
router.get(
  "/create-princess/done",
  princessController.princess_create_princess_get
);
router.get("/create-princess/step-1", princessController.princess_create_get);
router.post("/create-princess/step-1", princessController.princess_create_post);
router.get(
  "/create-princess/step-3",
  princessController.princess_create_costume_register_step3_get
);
router.post(
  "/create-princess-step-3",
  princessController.princess_create_costume_register_step3_post
);

router.post(
  "/create-princess-step-1",
  princessController.princess_create_costume_register_post
);
router.get("/homepage", princessController.princess_about_get);
router.post("/create-princess-step-3", princessController.princess_about_post);

router.get(
  "/homepage/edit-costume",
  princessController.princess_edit_costume_get
);
router.post(
  "/homepage/edit-costume",
  princessController.princess_edit_costume_post
);
router.get(
  "/homepage/edit-costume/save",
  princessController.princess_save_edit_costume_get
);
router.get(
  "/create-princess/step-2",
  princessController.princess_create_empiria_get
);

//city
router.get("/homepage/city", cityController.city_home_get);
router.get("/homepage/city/library", cityController.city_libraty_get);
router.get("/homepage/city/shopping-center", cityController.city_shopping_get);
router.post("/buy-glasses", cityController.buy_glasses_post);
router.post("/buy-socks", cityController.buy_socks_post);
router.get("/homepage/city/atm", cityController.city_atm_get);
router.post("/first-atm", cityController.first_atm_post);
router.get("/homepage/city/atm-save", cityController.atm_save_get);
router.post("/first-library", cityController.first_library_post);


//costumes
/*WARDROBE*/
router.get("/homepage/clothes", clothesController.clothes_wardrobe_get);
router.get("/homepage/clothes/head", clothesController.clothes_wardrobe_head);
router.get(
  "/homepage/clothes/middle",
  clothesController.clothes_wardrobe_middle
);
router.get(
  "/homepage/clothes/bottom",
  clothesController.clothes_wardrobe_bottom
);
router.get("/homepage/clothes/shoes", clothesController.clothes_wardrobe_shoes);
router.get(
  "/homepage/clothes/glasses",
  clothesController.clothes_wardrobe_glasses
);
router.get("/homepage/clothes/socks", clothesController.clothes_wardrobe_socks);

//ranking
router.get("/homepage/ranking", princessController.ranking_get);
router.get("/homepage/ranking/style", princessController.ranking_style_get);
router.get(
  "/homepage/ranking/creativity",
  princessController.ranking_creativity_get
);
router.get(
  "/homepage/ranking/intelligence",
  princessController.ranking_intelligence_get
);
router.get(
  "/homepage/ranking/condition",
  princessController.ranking_condition_get
);

module.exports = router;
