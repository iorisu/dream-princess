var mongoose = require("mongoose");
var validator = require("validator");
var bcrypt = require("bcrypt");

var Schema = mongoose.Schema;

var UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    max: 20
  },
  email: {
    type: String,
    validate: {
      validator: validator.isEmail,
      message: "{VALUE} is not a valid email",
      isAsync: false
    }
  },
  password: {
    type: String,
    required: true
  },
  passwordConf: {
    type: String,
    required: true
  },
  accountType: {
    type: String,
    default: "basic",
    required: true
  },
  namePrincess: {
    type: String,
    required: false,
    max: 12
  },
  skinColor: {
    type: String,
    enum: ["white", "brown", "black"],
    required: false
  },
  eyeColor: {
    type: String,
    enum: ["blue", "green", "brown"],
    required: false
  },
  hairColor: {
    type: String,
    enum: ["blonde", "brown", "black"],
    required: false
  },
  head: {
    type: String,
    required: false
  },
  middle: {
    type: String,
    required: false
  },
  bottom: {
    type: String,
    required: false
  },
  shoes: {
    type: String,
    required: false
  },
  style: {
    type: Number,
    default: 0
  },
  creativity: {
    type: Number,
    default: 0
  },
  Intelligence: {
    type: Number,
    default: 0
  },
  Condition: {
    type: Number,
    default: 0
  },
  dpMoney: {
    type: Number,
    default: "30",
    required: true
  },
  glassesBuy: {
    type: Boolean,
    default: false
  },
  socksBuy: {
    type: Boolean,
    default: false
  },
  glasses: {
    type: String,
    default: "none.png"
  },
  socks: {
    type: String,
    default: "none.png"
  },
  letsPlay: {
    type: Number
  },
  letsMoney: {
    type: Number
  }
});

//authenticate input against database
UserSchema.statics.authenticate = function(email, password, callback) {
  User.findOne({ email: email }).exec(function(err, user) {
    if (err) {
      return callback(err);
    } else if (!user) {
      var err = new Error("User not found.");
      err.status = 401;
      return callback(err);
    }
    bcrypt.compare(password, user.password, function(err, result) {
      if (result === true) {
        return callback(null, user);
      } else {
        return callback();
      }
    });
  });
};

//hashing a password before saving it to the database
UserSchema.pre("save", function(next) {
  var user = this;
  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  });
});

//Virtual URL
UserSchema.virtual("url").get(function() {
  return "/user/" + this._id;
});

var User = mongoose.model("User", UserSchema);
module.exports = User;
