var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ClothesSchema = new Schema({
  name: {
    type: String,
    required: true,
    max: 20
  },
  type: {
    type: String,
    required: true,
    max: 30
  },
  color: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  imageUrlMin: {
    type: String,
    required: true
  },
  style: {
    type: Number,
    default: 0
  },
  creativity: {
    type: Number,
    default: 0
  },
  Intelligence: {
    type: Number,
    default: 0
  },
  Condition: {
    type: Number,
    default: 0
  }
});

//Virtual
ClothesSchema.virtual("url").get(function() {
  return "/clothes/" + this._id;
});

module.exports = mongoose.model("Clothes", ClothesSchema);
