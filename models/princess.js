var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var PrincessSchema = new Schema({
  namePrincess: {
    type: String,
    required: true,
    max: 20
  },
  skinColor: {
    type: String,
    enum: ["white", "brown", "black"],
    required: true
  },
  eyeColor: {
    type: String,
    enum: ["blue", "green", "brown"],
    required: true
  },
  hairColor: {
    type: String,
    enum: ["blonde", "brown", "black"],
    required: true
  },
  costumes: {
    head: {
      type: String
    },
    middle: {
      type: String
    },
    bottom: {
      type: String
    },
    shoes: {
      type: String
    }
  },
  user: {
    type: Schema.ObjectId,
    ref: "User"
  }
});

//Virtual URL
PrincessSchema.virtual("url").get(function() {
  return "/princess/" + this._id;
});

module.exports = mongoose.model("Princess", PrincessSchema);
